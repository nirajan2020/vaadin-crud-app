package com.nirajanshrestha.spring.vaadin.UI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.nirajanshrestha.spring.vaadin.Entitiy.Item;
import com.nirajanshrestha.spring.vaadin.Service.ItemService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@SpringComponent
@UIScope
public class ItemsFormLogic extends ItemsForm {
	private Item item;
	private Grid<Item> itemGrid;
	private Div content;
	private Binder<Item> binder;
	private HorizontalLayout actions, operations;
	private VerticalLayout verticalLayout;
	private ItemService itemService;
	
	@Autowired
	public ItemsFormLogic(ItemService itemService) {
		this.itemService = itemService;
		item = new Item();
		itemGrid = new Grid<>(Item.class);
		binder = new Binder<>(Item.class);
		content = new Div(this.itemGrid); 
		actions = new HorizontalLayout(itemFilter, addNewItemBtn);
		operations = new HorizontalLayout(saveItemBtn, updateItemBtn, deleteItemBtn, cancelItemBtn);
		verticalLayout = new VerticalLayout();
		
		binder.bindInstanceFields(this);
		binder.setBean(item);
		
		content.setSizeFull();
		itemGrid.setHeight("200px");
		itemGrid.setColumns("item_id", "item_name");            
        
        verticalLayout.setSpacing(true);
        verticalLayout.add(item_name, operations);
        itemDialog.add(verticalLayout);
        
        add(actions, content);
        listItems(null);
        
		itemFilter.setValueChangeMode(ValueChangeMode.EAGER);
		itemFilter.addValueChangeListener(e -> listItems(e.getValue())); 
        
        addNewItemBtn.addClickListener(event -> addNewItem());
        saveItemBtn.addClickListener(event -> saveNewItem());
        updateItemBtn.addClickListener(event -> updateItem());
        deleteItemBtn.addClickListener(event -> deleteItem());
        cancelItemBtn.addClickListener(event -> cancel());
        
		itemGrid.addItemDoubleClickListener(listener-> 
        { 
        	editItem(listener.getItem());         
        	super.itemDialog.open();
         });
		
		itemGrid.addColumn(new ComponentRenderer<>(Item -> {
		    Button update = new Button("Edit", event -> {		
		    	binder.setBean(Item);
		    	itemDialog.open();
		    	saveItemBtn.setEnabled(false);
		    });

		    Button remove = new Button("Remove", event -> {
				this.itemService.removeItem(Item.getItem_id());
				listItems(null);
		    });

		    HorizontalLayout buttons = new HorizontalLayout(update, remove);
		    return new VerticalLayout( buttons);
		})).setHeader("Actions");
	}
	
	public void setItem(Item item) {
        binder.setBean(item);

        if (item == null) {
            setVisible(false);
        } else {
            setVisible(true);
            item_name.focus();
        }
    }
	
    private void addNewItem() {
    	editItem(new Item());
		itemDialog.open();
    }
    
    private void saveNewItem() {
        this.itemService.addItem(binder.getBean());
        listItems(null);
    	itemDialog.close();
    }
    
    private void updateItem() {
    	this.itemService.updateItem(binder.getBean());
    	listItems(null);
    	itemDialog.close();
    }
    
    private void cancel() {
    	System.out.println("Item Id: " + this.itemService.getItem("Bread").getItem_id());
		itemDialog.close();
    }
    
    private void deleteItem() {
    	this.itemService.removeItem(binder.getBean().getItem_id());
    	listItems(null);
    	itemDialog.close();
    }
    
//    private void ItemDoubleClick() {
//    	editItem(listener.getItem());         
//    	super.itemDialog.open();
//    }
	
	public Item editItem (Item item) {	
		if (item == null) {
			itemDialog.open();
			return null;
		}
		final boolean persisted = item.getItem_id() != null;
		if (persisted) {
			this.item = this.itemService.getItemById(item.getItem_id());
		}
		else {
			this.item = item;
		}
		
		this.binder.setBean(this.item);
		item_name.focus();
		itemDialog.add(item_name);
		return this.item;
	}
	
	void listItems(String filterText) {		
		if (StringUtils.isEmpty(filterText)) {
			itemGrid.setItems(this.itemService.listItem());			
			
	    } else {
	    	this.itemGrid.setItems(this.itemService.getItemByName(filterText));
	    }
    }
}
