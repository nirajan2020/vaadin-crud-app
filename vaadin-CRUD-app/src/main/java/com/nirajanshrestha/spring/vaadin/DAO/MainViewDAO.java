package com.nirajanshrestha.spring.vaadin.DAO;

import java.util.List;

import com.nirajanshrestha.spring.vaadin.Entitiy.Customer;

public interface MainViewDAO {
	public List<Object[]> listCustomer();
	public List<Object[]> getCustomerByName(String text);
	public Customer getCustomerById(Long id);
	public Object[] getCustomer(String name);
	public void removeCustomer(Long id);
}
