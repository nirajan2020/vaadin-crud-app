package com.nirajanshrestha.spring.vaadin.UI;

public enum CustomerStatus {
    ImportedLead, NotContacted, Contacted, Customer, ClosedLost
}
