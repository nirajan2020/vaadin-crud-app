package com.nirajanshrestha.spring.vaadin.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nirajanshrestha.spring.vaadin.Entitiy.Customer;

@Repository
@Transactional
public class MainViewDAOImpl implements MainViewDAO{	

	private static final Logger logger = LoggerFactory.getLogger(ItemDAOImpl.class);
	private int result;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public MainViewDAOImpl(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}

	@Override
	public List<Object[]> listCustomer() {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_customer");
		return query.getResultList();
	}

	@Override
	public List<Object[]> getCustomerByName(String text) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_customer where first_name LIKE "+ "'%" + text + "%'");
		return query.getResultList();
	}

	@Override
	public Customer getCustomerById(Long id) {
		return this.entityManager.find(Customer.class, id);
	}

	@Override
	public Object[] getCustomer(String name) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_customer where first_name = "+ "'" + name + "'");
		return (Object[]) query.getSingleResult();
	}

	@Override
	public void removeCustomer(Long id) {
		Customer customer = this.entityManager.find(Customer.class, id);
		this.entityManager.remove(customer);
		
	}

}
