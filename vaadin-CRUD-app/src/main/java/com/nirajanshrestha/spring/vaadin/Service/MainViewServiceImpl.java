package com.nirajanshrestha.spring.vaadin.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nirajanshrestha.spring.vaadin.DAO.MainViewDAO;
import com.nirajanshrestha.spring.vaadin.Entitiy.Customer;
import com.nirajanshrestha.spring.vaadin.UI.CustomerStatus;

@Service
public class MainViewServiceImpl implements MainViewService{

	@Autowired
	private MainViewDAO mainViewDAO;
	
	List<Customer> customers = new ArrayList<Customer>();
	
	@Override
	public List<Customer> listCustomer() {
		customers.clear();		
		for (Object[] customer : mainViewDAO.listCustomer()) {		
			Customer customerEntity = new Customer();
			customerEntity.setId(Long.parseLong(customer[0].toString()));
			customerEntity.setEmail(customer[1].toString());
			customerEntity.setFirstName(customer[2].toString());
			customerEntity.setLastName(customer[3].toString());
			customerEntity.setStatus(CustomerStatus.values()[(int) customer[4]]);
			
			customers.add(customerEntity);
		}
		return customers;
	}

	@Override
	public List<Customer> getCustomerByName(String text) {
		customers.clear();		
		for (Object[] customer : mainViewDAO.getCustomerByName(text)) {		
			Customer customerEntity = new Customer();
			customerEntity.setId(Long.parseLong(customer[0].toString()));
			customerEntity.setEmail(customer[1].toString());
			customerEntity.setFirstName(customer[2].toString());
			customerEntity.setLastName(customer[3].toString());
			customerEntity.setStatus(CustomerStatus.values()[(int) customer[4]]);
			
			customers.add(customerEntity);
		}
		return customers;
	}

	@Override
	public Customer getCustomerById(Long id) {		
		return mainViewDAO.getCustomerById(id);
	}

	@Override
	public Customer getCustomer(String name) {
		Customer customerEntity = new Customer();
		Object[] customer = mainViewDAO.getCustomer(name);
		customerEntity.setId(Long.parseLong(customer[0].toString()));
		customerEntity.setEmail(customer[1].toString());
		customerEntity.setFirstName(customer[2].toString());
		customerEntity.setLastName(customer[3].toString());
		customerEntity.setStatus(CustomerStatus.values()[(int) customer[4]]);
		return customerEntity;
	}

	@Override
	public void removeCustomer(Long id) {
		mainViewDAO.removeCustomer(id);
		
	}

}
