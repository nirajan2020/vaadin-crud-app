package com.nirajanshrestha.spring.vaadin.Entitiy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_item")
public class Item {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "item_id", unique = true, nullable = false)
	private Long item_id;  
	
	@Column(name = "item_name", unique = false, nullable = false, length = 250)
	private String item_name;
	
//	@OneToOne(mappedBy="item")
//	private Customer customer;
//	
//	public Customer getCustomer() {
//		return customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}

	public Item() {
		
	}
	
	public Item(String item_name) {
		this.item_name = item_name;
	}
	
	public Long getItem_id() {
		return item_id;
	}

	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
}
