package com.nirajanshrestha.spring.vaadin.UI;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@Route("itemFormView")
public class ItemsForm extends VerticalLayout{

	TextField itemFilter, item_name;
    Button addNewItemBtn, saveItemBtn, updateItemBtn, cancelItemBtn, deleteItemBtn;
    Dialog itemDialog;
    
    public ItemsForm() {
    	itemFilter = new TextField();
    	item_name = new TextField("Item Name");
    	
    	addNewItemBtn = new Button("Add New Item");
    	saveItemBtn = new Button("Save");
    	updateItemBtn = new Button("Update");
    	cancelItemBtn = new Button("Cancel");
    	deleteItemBtn = new Button("Delete");
    	
    	itemDialog = new Dialog();
    	itemFilter.setPlaceholder("Search by item name");
	}
}
