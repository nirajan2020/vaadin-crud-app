package com.nirajanshrestha.spring.vaadin.UI;

import org.springframework.util.StringUtils;

import com.nirajanshrestha.spring.vaadin.Entitiy.Customer;
import com.nirajanshrestha.spring.vaadin.Entitiy.Item;
import com.nirajanshrestha.spring.vaadin.Service.CustomerServiceInterface;
import com.nirajanshrestha.spring.vaadin.Service.ItemService;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@SpringComponent
@UIScope
public class CustomerFormLogic extends CustomerForm{

    Binder<Customer> binder = new Binder<>(Customer.class);
    private ComboBox<CustomerStatus> status = new ComboBox<>("Status");
    private ComboBox<Item> item = new ComboBox<>("Item");
    private MainViewLogic mainViewLogic;
    private CustomerServiceInterface customerServiceInterface;
    private ItemService itemService;
    private VerticalLayout vLayout;
    private Item itemEntitiy;
    
	public CustomerFormLogic(MainViewLogic mainViewLogic, CustomerServiceInterface customerServiceInterface, ItemService itemService) {
        this.mainViewLogic = mainViewLogic;
        this.customerServiceInterface = customerServiceInterface;
        this.itemService = itemService;
        status.setItems(CustomerStatus.values());
        vLayout = new VerticalLayout();
        	
        HorizontalLayout buttons = new HorizontalLayout(save, update, delete, cancel);
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        vLayout.add(firstName, lastName, email, status, item, buttons);
        customerFormDialog.add(vLayout);

        binder.bindInstanceFields(this);
        populateComboBox();

        save.addClickListener(event -> save());
        delete.addClickListener(event -> delete());
        update.addClickListener(event -> update());
        cancel.addClickListener(event -> cancel());
	}
	
	public void setCustomer(Customer customer) {
        binder.setBean(customer);
        if (customer == null) {
            setVisible(false);
        } else {
            setVisible(true);
            firstName.focus();
        }
    }

    private void save() {
        Customer customer = binder.getBean();
        /** for setting up foreign key **/
//		if(!item.getValue().equals(null)) {
//			itemEntitiy = new Item(item.getValue().getItem_name());  
//			itemEntitiy.setItem_id(this.itemService.getItem(item.getValue().getItem_name()).getItem_id());
//			customer.setItem(itemEntitiy);
//		}
		/** ----------------------------------- **/
        customerServiceInterface.addCustomer(customer);
        listCustomerDetails(null);        
        customerFormDialog.close();
        setCustomer(null);
    }

    private void delete() {
        Customer customer = binder.getBean();
        customerServiceInterface.removeCustomer(customer.getId());
        listCustomerDetails(null);        
        customerFormDialog.close();
        setCustomer(null);
    }
    
    private void update() {
        Customer customer = binder.getBean();
        customerServiceInterface.updateCustomer(customer);
        listCustomerDetails(null);        
        customerFormDialog.close();
        setCustomer(null);
    }
    
    private void cancel() {
    	customerFormDialog.close();
    }
    
	public void listCustomerDetails(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			mainViewLogic.grid.setItems(this.customerServiceInterface.listCustomer());			
			
	    } else {
	    	mainViewLogic.grid.setItems(this.customerServiceInterface.getCustomerByName(filterText));
	    }
    }
	
   public void populateComboBox() {		
	ListDataProvider<Item> dataProviderItem = new ListDataProvider<Item>(this.itemService.listItem());
	dataProviderItem.setSortOrder(Item::getItem_name, SortDirection.ASCENDING);					
	item.setItemLabelGenerator(Item::getItem_name);	
	item.setDataProvider(dataProviderItem);	
   }
}
