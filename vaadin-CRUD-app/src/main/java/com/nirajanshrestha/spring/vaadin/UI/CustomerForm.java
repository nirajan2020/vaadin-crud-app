package com.nirajanshrestha.spring.vaadin.UI;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

public class CustomerForm extends FormLayout {

    protected TextField firstName, lastName, email;
    protected Button save, update, cancel;
    protected Button delete;
    Dialog customerFormDialog;

    public CustomerForm() {
    	firstName = new TextField("First name");
    	lastName = new TextField("Last name");
    	email = new TextField("Email");
    	save = new Button("Save");
    	update = new Button("Update");
    	delete = new Button("Delete");  
    	cancel = new Button("Cancel"); 
    	customerFormDialog = new Dialog();
    }
}
