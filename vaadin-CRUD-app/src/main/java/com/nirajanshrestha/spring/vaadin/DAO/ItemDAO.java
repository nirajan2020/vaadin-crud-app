package com.nirajanshrestha.spring.vaadin.DAO;

import java.util.List;

import com.nirajanshrestha.spring.vaadin.Entitiy.Item;

public interface ItemDAO {
	public void addItem(Item item);
	public void updateItem(Item item);
	public List<Item> listItem();
	public List<Item> getItemByName(String text);
	public Item getItemById(Long id);
	public Item getItem(String name);
	public void removeItem(Long id);
}
