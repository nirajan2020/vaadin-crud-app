package com.nirajanshrestha.spring.vaadin.UI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.nirajanshrestha.spring.vaadin.Entitiy.Customer;
import com.nirajanshrestha.spring.vaadin.Entitiy.Item;
import com.nirajanshrestha.spring.vaadin.Service.CustomerServiceInterface;
import com.nirajanshrestha.spring.vaadin.Service.ItemService;
import com.nirajanshrestha.spring.vaadin.Service.MainViewService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@SpringComponent
@UIScope
public class MainViewLogic extends MainView {
	
	private Customer customer;
    Grid<Customer> grid = new Grid<>(Customer.class);
    private TextField filterText = new TextField();
    private CustomerServiceInterface customerServiceInterface;
    private ItemService itemService;
    MainViewService mainViewService;
    private CustomerFormLogic form;
    private Button addCustomerBtn, goToItemViewBtn;
    
	public MainViewLogic(CustomerServiceInterface customerServiceInterface, ItemService itemService, MainViewService mainViewService) {
		this.customerServiceInterface = customerServiceInterface;
		this.itemService = itemService;
		this.mainViewService = mainViewService;
		customer = new Customer();
		form = new CustomerFormLogic(this, this.customerServiceInterface, this.itemService);
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);

		addCustomerBtn = new Button("Add new customer");  
		goToItemViewBtn = new Button("Go to Item View");

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addCustomerBtn, goToItemViewBtn);

        grid.setColumns("id", "firstName", "lastName", "status", "email");
        grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);	
        
        HorizontalLayout mainContent = new HorizontalLayout(grid, form);
        mainContent.setSizeFull();
        grid.setSizeFull();

        add(toolbar, mainContent);
        setSizeFull();        
        form.setCustomer(null);
        populateGrid(null);
        
        filterText.setValueChangeMode(ValueChangeMode.EAGER);
        filterText.addValueChangeListener(e -> populateGrid(e.getValue()));
        
        addCustomerBtn.addClickListener(e -> addNewCustomer());
        goToItemViewBtn.addClickListener(e -> navigateToItemView());
        
        /** for editing and deleting of the grid rows **/
        grid.addColumn(new ComponentRenderer<>(Customer -> {        	
		    Button update = new Button("Edit", event -> {
		    	form.binder.setBean(Customer);
		    	form.customerFormDialog.open();
		    	form.save.setEnabled(false);
		    });
		    Button remove = new Button("Remove", event -> {
				this.customerServiceInterface.removeCustomer(Customer.getId());
				populateGrid(null);
		    });

		    HorizontalLayout buttons = new HorizontalLayout(update, remove);
		    return new VerticalLayout( buttons);
		})).setHeader("Actions");
        /** --------------------------------------------------------------------- **/
        
        /** for double clicking on the grid rows **/
		grid.addItemDoubleClickListener(listener-> 
        { 
	    	form.binder.setBean(listener.getItem());
	    	form.customerFormDialog.open();
	    	form.save.setEnabled(false);
         });
        
	}
	public void addNewCustomer() {	  
	  grid.asSingleSelect().clear();
      form.setCustomer(new Customer());
      form.save.setEnabled(true);
      form.customerFormDialog.open();
	}
	
	public void navigateToItemView() {
		UI.getCurrent().navigate("itemFormView");
	}
	
	void populateGrid(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			grid.setItems(this.mainViewService.listCustomer());			
			
	    } else {
	    	grid.setItems(this.mainViewService.getCustomerByName(filterText));
	    }
    }
}
