package com.nirajanshrestha.spring.vaadin.Main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.flow.spring.annotation.EnableVaadin;

@SpringBootApplication
@ComponentScan(basePackages = "com.nirajanshrestha.spring.*") // important
@EntityScan("com.nirajanshrestha.spring.*") //important  
@EnableVaadin({"com.nirajanshrestha.spring.vaadin.UI"}) // important
public class VaadinCrudAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaadinCrudAppApplication.class, args);
	}

}
