package com.example.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.DAO.DesignationDAO;
import com.example.spring.entities.Department;
import com.example.spring.entities.Designation;

@Service
public class DesignationServiceImpl implements DesignationService {

	@Autowired
	private DesignationDAO designationDAO;
	
	List<Designation> designations = new ArrayList<Designation>();
	
	@Override
	public void addDesignation(Designation designation) {
		designationDAO.addDesignation(designation);
	}

	@Override
	public void updateDesignation(Designation designation) {
		this.designationDAO.updateDesignation(designation);		
	}

	@Override
	public List<Designation> listDesignation() {
		designations.clear();
		for (Object[] emp : designationDAO.listDesignation()) {
			Designation designationEntity = new Designation();
			designationEntity.setDesignation_id(Long.parseLong(emp[0].toString()));
			designationEntity.setDesignation_name(emp[1].toString());
//			System.out.println("Designation Name: " + designationEntity.getDesignation_name());
			designations.add(designationEntity);
		}
		return designations;
	}

	@Override
	public List<Designation> getDesignationByName(String text) {
		designations.clear();
		
		for (Object[] emp : designationDAO.getDesignationByName(text)) {	
			Designation designationEntity = new Designation();
			designationEntity.setDesignation_id(Long.parseLong(emp[0].toString()));
			designationEntity.setDesignation_name(emp[1].toString());
			designations.add(designationEntity);
		}
		return designations;
	}

	@Override
	public Designation getDesignationById(Long id) {
		return designationDAO.getDesignationById(id);
	}

	@Override
	public void removeDesignation(Long id) {
		this.designationDAO.removeDesignation(id);
		
	}

	@Override
	public Designation getDesignation(String name) {
		Designation designationEntity = new Designation();
		Object[] emp = designationDAO.getDesignation(name);			
		designationEntity.setDesignation_id(Long.parseLong(emp[0].toString()));
		designationEntity.setDesignation_name(emp[1].toString());
		
		return designationEntity;
	}

	
}
