package com.example.spring.service;

import java.util.List;

import com.example.spring.entities.Employee;

public interface NewEmployeeService {
	public void addEmployee(Employee employee);
	public void updateEmployee(Employee employee);
	public List<Employee> getEmployees();
	public List<Employee> getEmployeesByName(String text);
	public Employee getEmployeeById(Long id);
	public void removeEmployee(Long id);
}
