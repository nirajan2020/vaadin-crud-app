package com.example.spring.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.DAO.EmployeeDAO;
import com.example.spring.entities.Employee;
//import com.example.spring.entities.Employee.comboBoxOptions;
import com.example.spring.entities.Employee.options;

@Service
public class NewEmployeeServiceImpl implements NewEmployeeService {
	@Autowired
	private EmployeeDAO employeeDAO;
	
	List<Employee> employees = new ArrayList<Employee>();
	
	@Override
	public void addEmployee(Employee employee) {
		this.employeeDAO.addEmployee(employee);		
	}

	@Override
	public void updateEmployee(Employee employee) {
		this.employeeDAO.updateEmployee(employee);		
	}

	@Override
	public List<Employee> getEmployees() {
		this.employees.clear();
		for (Object[] emp : this.employeeDAO.listEmployees()) {
			Employee employeeEntity = new Employee();
			employeeEntity.setId(Long.parseLong(emp[0].toString()));
			employeeEntity.setName(emp[1].toString());
			employeeEntity.setSalary(Double.parseDouble(emp[2].toString()));
			employeeEntity.setSalutation(Enum.valueOf(options.class, emp[3].toString())); // Enum to string
			
//			employeeEntity.setDesignation_name(emp[4].toString());
//			employeeEntity.setDepartment_name(emp[5].toString());
//			employeeEntity.setDesignation(Arrays.asList(emp[4].toString()));

			employeeEntity.setDescription(emp[4].toString());
			
			this.employees.add(employeeEntity);
		}		
		return this.employees;
	}

	@Override
	public Employee getEmployeeById(Long id) {		
		return this.employeeDAO.getEmployeeById(id);
	}

	@Override
	public void removeEmployee(Long id) {
		this.employeeDAO.removeEmployee(id);		
	}

	@Override
	public List<Employee> getEmployeesByName(String text) {	
		this.employees.clear();
		for (Object[] emp : this.employeeDAO.getEmployeesByName(text)) {
			Employee employeeEntity = new Employee();
			employeeEntity.setId(Long.parseLong(emp[0].toString()));
			employeeEntity.setName(emp[1].toString());
			employeeEntity.setSalary(Double.parseDouble(emp[2].toString()));
			employeeEntity.setSalutation(Enum.valueOf(options.class, emp[3].toString())); // Enum to string
//			employeeEntity.setDesignation(Enum.valueOf(comboBoxOptions.class, emp[4].toString()));
//			employeeEntity.setDesignation_name(emp[4].toString());
			employeeEntity.setDescription(emp[4].toString());
			
			this.employees.add(employeeEntity);
		}		
		return this.employees;
	}

}
