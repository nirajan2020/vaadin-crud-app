package com.example.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.DAO.DepartmentDAO;
import com.example.spring.entities.Department;
import com.example.spring.entities.Designation;


@Service
public class DepartmentServiceImpl implements DepartmentService{
	
	@Autowired
	private DepartmentDAO departmentDAO;
	
	List<Department> departments = new ArrayList<Department>();

	@Override
	public void addDepartment(Department department) {
		this.departmentDAO.addDepartment(department);
		
	}

	@Override
	public void updateDepartment(Department department) {
		this.departmentDAO.updateDepartment(department);
		
	}

	@Override
	public List<Department> listDepartment() {
		departments.clear();		
		for (Object[] emp : departmentDAO.listDepartment()) {		
			Department departmentEntity = new Department();
			departmentEntity.setDepartment_id(Long.parseLong(emp[0].toString()));
			departmentEntity.setDepartment_name(emp[1].toString());
//			System.out.println("Department Name: " + departmentEntity.getDepartment_name());
			departments.add(departmentEntity);
		}
		return departments;
	}

	@Override
	public List<Department> getDepartmentByName(String text) {
		departments.clear();		
		for (Object[] emp : departmentDAO.getDepartmentByName(text)) {	
			Department departmentEntity = new Department();
			departmentEntity.setDepartment_id(Long.parseLong(emp[0].toString()));
			departmentEntity.setDepartment_name(emp[1].toString());
			departments.add(departmentEntity);
		}
		return departments;
	}

	@Override
	public Department getDepartmentById(Long id) {
		return this.departmentDAO.getDepartmentById(id);
	}

	@Override
	public void removeDepartment(Long id) {
		this.departmentDAO.removeDepartment(id);
		
	}

	@Override
	public Department getDepartment(String name) {		
		Department departmentEntity = new Department();
		Object[] emp = departmentDAO.getDepartment(name);			
		departmentEntity.setDepartment_id(Long.parseLong(emp[0].toString()));
		departmentEntity.setDepartment_name(emp[1].toString());
		
		return departmentEntity;
	}

}
