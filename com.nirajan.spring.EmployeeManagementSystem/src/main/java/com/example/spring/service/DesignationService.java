package com.example.spring.service;

import java.util.List;

import com.example.spring.entities.Department;
import com.example.spring.entities.Designation;

public interface DesignationService {
	public void addDesignation(Designation designation);
	public void updateDesignation(Designation designation);
	public List<Designation> listDesignation();
	public List<Designation> getDesignationByName(String text);
	public Designation getDesignationById(Long id);
	public Designation getDesignation(String name);
	public void removeDesignation(Long id);
}
