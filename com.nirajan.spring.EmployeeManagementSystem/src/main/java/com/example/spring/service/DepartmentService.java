package com.example.spring.service;

import java.util.List;

import com.example.spring.entities.Department;

public interface DepartmentService {
	public void addDepartment(Department Department);
	public void updateDepartment(Department Department);
	public List<Department> listDepartment();
	public List<Department> getDepartmentByName(String text);
	public Department getDepartmentById(Long id);
	public Department getDepartment(String name);
	public void removeDepartment(Long id);
}
