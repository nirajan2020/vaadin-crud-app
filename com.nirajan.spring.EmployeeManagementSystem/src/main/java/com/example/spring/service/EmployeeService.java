package com.example.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.entities.Employee;
import com.example.spring.entities.EmployeeRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManagerFactory;

@Service
public class EmployeeService {
	private static final Logger LOGGER = Logger.getLogger(Employee.class.getName());
	@Autowired
    private EmployeeRepository employeeRepository;	
  
	public EmployeeService(EmployeeRepository repo) {
		this.employeeRepository = repo;	
	}
	
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}
	
	public List<Employee> search(String searchTerm) {
		return employeeRepository.search(searchTerm);
	}
	
	public List<Employee> findByName(String filterText) {
		return employeeRepository.findByNameStartsWithIgnoreCase(filterText);
	}

	public long count() {
		return employeeRepository.count();
	}

	public void delete(Employee employee) {
		if (employee == null) { 
			LOGGER.log(Level.SEVERE,
					"Employee is null. Are you sure you have connected your form to the application?");
			return;
		}
		employeeRepository.delete(employee);
	}

	public void save(Employee employee) {
		if (employee == null) { 
			LOGGER.log(Level.SEVERE,
					"Employee is null. Are you sure you have connected your form to the application?");
			return;
		}
		employeeRepository.save(employee);
	}	
}
