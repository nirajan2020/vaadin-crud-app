package com.example.spring.DAO;

import java.util.List;

import com.example.spring.entities.Employee;

public interface EmployeeDAO {
	public void addEmployee(Employee employee);
	public void updateEmployee(Employee employee);
	public List<Object[]> listEmployees();
	public List<Object[]> getEmployeesByName(String text);
	public Employee getEmployeeById(Long id);
	public void removeEmployee(Long id);
}
