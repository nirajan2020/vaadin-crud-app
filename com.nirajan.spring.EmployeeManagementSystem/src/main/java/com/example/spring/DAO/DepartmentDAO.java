package com.example.spring.DAO;

import java.util.List;

import com.example.spring.entities.Department;

public interface DepartmentDAO {
	public void addDepartment(Department Department);
	public void updateDepartment(Department Department);
	public List<Object[]> listDepartment();
	public List<Object[]> getDepartmentByName(String text);
	public Department getDepartmentById(Long id);
	public Object[] getDepartment(String name);
	public void removeDepartment(Long id);
}
