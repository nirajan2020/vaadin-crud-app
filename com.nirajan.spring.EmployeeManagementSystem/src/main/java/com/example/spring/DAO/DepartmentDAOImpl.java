package com.example.spring.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.spring.entities.Department;


@Repository
@Transactional
public class DepartmentDAOImpl implements DepartmentDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	private int result;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public DepartmentDAOImpl(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}

	@Override
	public void addDepartment(Department department) {
		this.entityManager.persist(department);		
	}

	@Override
	public void updateDepartment(Department department) {
		this.entityManager.merge(department);		
	}

	@Override
	public List<Object[]> listDepartment() {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_department");
		return query.getResultList();
	}

	@Override
	public List<Object[]> getDepartmentByName(String text) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_department where department_name LIKE "+ "'%" + text + "%'");
		return query.getResultList();
	}

	@Override
	public Department getDepartmentById(Long id) {
		return this.entityManager.find(Department.class, id);
	}
	
	@Override
	public void removeDepartment(Long id) {
		Department department = this.entityManager.find(Department.class, id);
		this.entityManager.remove(department);		
	}

	@Override
	public Object[] getDepartment(String name) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_department where department_name = "+ "'" + name + "'");
		return (Object[]) query.getSingleResult();
	}

}
