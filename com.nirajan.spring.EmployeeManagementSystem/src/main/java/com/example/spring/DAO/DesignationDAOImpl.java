package com.example.spring.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.spring.entities.Designation;
import com.example.spring.entities.Employee;


@Repository
@Transactional
public class DesignationDAOImpl implements DesignationDAO {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	private int result;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public DesignationDAOImpl(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	@Override
	public List<Object[]> listDesignation() {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_designation");
		return query.getResultList();
	}

	@Override
	public List<Object[]> getDesignationByName(String text) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_designation where designation_name LIKE "+ "'%" + text + "%'");
		return query.getResultList();
	}

	@Override
	public Designation getDesignationById(Long id) {
//		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_designation where designation_id="+ "'" + id + "'");
//		return query.getResultList();
		return this.entityManager.find(Designation.class, id);
	}

	@Override
	public void removeDesignation(Long id) {
		Designation designation = this.entityManager.find(Designation.class, id);
		this.entityManager.remove(designation);
	}

	@Override
	public void addDesignation(Designation designation) {
//		this.entityManager.persist(employee);
		
		result = this.entityManager.createNativeQuery("INSERT INTO tbl_designation (designation_name) VALUES (?)")
	      .setParameter(1, designation.getDesignation_name())
	      .executeUpdate();
		
		if(result > 0) {
			logger.info("New Designation Insertion Successful");
		}
		else {
			logger.info("New Designation Insertion Failed");
		}
	}

	@Override
	public void updateDesignation(Designation designation) {
		this.entityManager.merge(designation);
		
	}

	@Override
	public Object[] getDesignation(String name) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM tbl_designation where designation_name = "+ "'" + name + "'");
		return (Object[]) query.getSingleResult();
	}

}
