package com.example.spring.DAO;

import java.util.List;

import com.example.spring.entities.Designation;

public interface DesignationDAO {
	public void addDesignation(Designation designation);
	public void updateDesignation(Designation designation);
	public List<Object[]> listDesignation();
	public List<Object[]> getDesignationByName(String text);
	public Designation getDesignationById(Long id);
	public Object[] getDesignation(String name);
	public void removeDesignation(Long id);
}
