package com.example.spring.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.spring.entities.Employee;

@Repository
@Transactional
public class EmployeeDAOImpl implements EmployeeDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public EmployeeDAOImpl(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	@Override
	public void addEmployee(Employee employee) {
		/** Mannual query definition  **/
//		employee.setId(1);
//		this.entityManager.createNativeQuery("INSERT INTO employee (salutation, name, salary, designation_name, description, designationId) VALUES (?,?,?,?,?,?)")
//	      .setParameter(1, employee.getSalutation().toString())
//	      .setParameter(2, employee.getName())
//	      .setParameter(3, employee.getSalary())
//	      .setParameter(4, employee.getDesignation_name())
//	      .setParameter(5, employee.getDescription())
//	      .setParameter(6, employee.getId())
//	      .executeUpdate();
		
		this.entityManager.persist(employee);	
		
//		logger.info("Employee saved successfully, Employee Details="+employee.toString());
	}

	@Override
	public void updateEmployee(Employee employee) {
		this.entityManager.merge(employee);	
	}

	@Override
	public List<Object[]> listEmployees() {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM employee");
		return query.getResultList();
	}

	@Override
	public Employee getEmployeeById(Long id) {		
		return this.entityManager.find(Employee.class, id);
	}

	@Override
	public void removeEmployee(Long id) {
		Employee employee = this.entityManager.find(Employee.class, id);
		this.entityManager.remove(employee);		
	}

	@Override
	public List<Object[]> getEmployeesByName(String text) {
		Query query = this.entityManager.createNativeQuery("SELECT * FROM employee WHERE name LIKE"+ "'%" + text + "%'");
		return query.getResultList();
	}

}
