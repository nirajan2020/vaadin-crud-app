package com.example.spring.UI;

import org.springframework.util.StringUtils;

import com.example.spring.entities.Department;
import com.example.spring.entities.Designation;
import com.example.spring.entities.Employee;
import com.example.spring.service.DepartmentService;
import com.example.spring.service.DesignationService;
import com.example.spring.service.NewEmployeeService;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.SortDirection;

public class EmployeeEditorLogic extends EmployeeEditor{
	Binder<Employee> binder;
	NewEmployeeService employeeService;
	DesignationService designationService;
	DepartmentService departmentService;
	Designation designation;
	Department department;
	Employee employee;
	MainViewLogic mainViewLogic;
	
	public EmployeeEditorLogic() {
		
	}
	
	public EmployeeEditorLogic(NewEmployeeService employeeService, DesignationService designationService, DepartmentService departmentService,MainViewLogic mainViewLogic) {
		this.employeeService = employeeService;
		this.designationService = designationService;
		this.departmentService = departmentService;
		this.mainViewLogic = mainViewLogic;
		binder = new Binder<>(Employee.class);
		binder.bindInstanceFields(this);		
		
		save.addClickListener(e -> {
			employee = binder.getBean();
			
			if(!designation_name.getValue().equals(null)) {
				designation = new Designation(designation_name.getValue().getDesignation_name());  
				designation.setDesignation_id(this.designationService.getDesignation(designation_name.getValue().getDesignation_name()).getDesignation_id());
				employee.setDesignation(designation);
			}
			if(!department_name.getValue().equals(null)) {
				department = new Department(department_name.getValue().getDepartment_name()); 
				department.setDepartment_id(this.departmentService.getDepartment(department_name.getValue().getDepartment_name()).getDepartment_id());
				employee.setDepartment(department);
			}
			// important for OneToMany relations //
//			designation.setEmployees(Arrays.asList(employee));
//			department.setEmployees(Arrays.asList(employee));			
			
			// -----------------------------------//
			
        	this.employeeService.addEmployee(employee); // calling a Service interface
        	listEmployees(null); 
        	dialog.close();
        	});		
		
		update.addClickListener(e -> {
			employee = binder.getBean();
			this.employeeService.updateEmployee(employee);
			listEmployees(null);
			dialog.close();
		});
		
		delete.addClickListener(e -> {
			if(employee.getId() != null) {
				this.employeeService.removeEmployee(employee.getId());
				listEmployees(null);
				dialog.close();
			}

		});
		
		cancel.addClickListener(e -> {
			dialog.close();
        });		
		
	}	
	
	public Employee editEmployee (Employee emp) {	
		if (emp == null) {
			vLayout.setVisible(false);
			return null;
		}
		final boolean persisted = emp.getId() != null;
		if (persisted) {
			employee = this.employeeService.getEmployeeById(emp.getId());
		}
		else {
			employee = emp;
		}
		
		binder.setBean(employee);
		vLayout.setVisible(true);
		name.focus();
		dialog.add(vLayout);
		return employee;
	}
	
   public void listEmployees(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			mainViewLogic.grid.setItems(this.employeeService.getEmployees());			
			
	    } else {
	    	mainViewLogic.grid.setItems(this.employeeService.getEmployeesByName(filterText));
	    }
    }	
    
   public void populateComboBox() {
	editEmployee(new Employee("", 0.0, Employee.options.Mr, null, null, null));				
		
	ListDataProvider<Designation> dataProviderDesignation = new ListDataProvider<Designation>(this.designationService.listDesignation());
	dataProviderDesignation.setSortOrder(Designation::getDesignation_name, SortDirection.ASCENDING);					
	designation_name.setItemLabelGenerator(Designation::getDesignation_name);	
	designation_name.setDataProvider(dataProviderDesignation);		
		
	ListDataProvider<Department> dataProviderDepartment = new ListDataProvider<Department>(this.departmentService.listDepartment());
	dataProviderDepartment.setSortOrder(Department::getDepartment_name, SortDirection.ASCENDING);					
	department_name.setItemLabelGenerator(Department::getDepartment_name);
	department_name.setDataProvider(dataProviderDepartment);	
	    				        
	dialog.open();
   }

}
