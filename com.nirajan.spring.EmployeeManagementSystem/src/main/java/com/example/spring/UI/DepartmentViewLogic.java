package com.example.spring.UI;

import org.springframework.util.StringUtils;

import com.example.spring.entities.Department;
import com.example.spring.service.DepartmentService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;

public class DepartmentViewLogic extends DepartmentView{

	private DepartmentService departmentService;
	private Department department;
	private Grid<Department> departmentGrid;
	private Div content;
	private Binder<Department> binder;
	private HorizontalLayout actions, operations;
	private VerticalLayout verticalLayout;
	
	public DepartmentViewLogic(DepartmentService departmentService) {
		this.departmentService = departmentService;
		this.department = new Department(null);
		this.departmentGrid = new Grid<>(Department.class);
		this.binder = new Binder<>(Department.class);
		this.content = new Div(this.departmentGrid); 
		this.actions = new HorizontalLayout(super.departmentFilter, super.addNewDepartmentBtn);
		this.operations = new HorizontalLayout(super.addBtn, super.updateBtn, super.deleteBtn, super.cancelBtn);
		this.verticalLayout = new VerticalLayout();
		
		this.binder.bindInstanceFields(this);
		this.binder.setBean(this.department);
		
        this.content.setSizeFull();
        this.departmentGrid.setHeight("200px");
        this.departmentGrid.setColumns("department_id", "department_name");            
        
        this.verticalLayout.setSpacing(true);
        this.verticalLayout.add(super.department_name, this.operations);
        super.departmentDialog.add(this.verticalLayout);
        
        add(this.actions, this.content);
		
		listDepartments(null);
		
		super.departmentFilter.setValueChangeMode(ValueChangeMode.EAGER);// read the api
		super.departmentFilter.addValueChangeListener(e -> listDepartments(e.getValue())); 
		
		super.addNewDepartmentBtn.addClickListener(e -> {	
			editDepartment(new Department(null));
			super.departmentDialog.open();
		});
		
		super.addBtn.addClickListener(e -> {				
			this.departmentService.addDepartment(this.department);
			listDepartments(null);
			super.departmentDialog.close();
		});
		
		super.updateBtn.addClickListener(e -> {	
			this.departmentService.updateDepartment(this.department);
			listDepartments(null);
			super.departmentDialog.close();
		});
		
		super.cancelBtn.addClickListener(e -> {				
			super.departmentDialog.close();
		});
		
		super.deleteBtn.addClickListener(e -> {	
			if(this.department.getDepartment_id() != null) {
				this.departmentService.removeDepartment(this.department.getDepartment_id());
				listDepartments(null);
				super.departmentDialog.close();
			}

		});
		
		this.departmentGrid.addColumn(new ComponentRenderer<>(Department -> {
		    // button for updating an employee details to backend
			editDepartment(Department); 
		    Button update = new Button("Edit", event -> {		    		    	
		    	super.departmentDialog.open();
		    	super.addBtn.setEnabled(false);
		    });

		    // button that removes an employee 
		    Button remove = new Button("Remove", event -> {
				this.departmentService.removeDepartment(this.department.getDepartment_id());
				listDepartments(null);
		    });

		    // layouts for placing the text field on top of the buttons
		    HorizontalLayout buttons = new HorizontalLayout(update, remove);
		    return new VerticalLayout( buttons);
		})).setHeader("Actions");
		
		this.departmentGrid.addItemDoubleClickListener(listener-> 
        { 
        	editDepartment(listener.getItem());         
        	super.departmentDialog.open();
         });
	}
	
	public Department editDepartment (Department des) {	
		if (des == null) {
			super.departmentDialog.open();
			return null;
		}
		final boolean persisted = des.getDepartment_id() != null;
		if (persisted) {
			this.department = this.departmentService.getDepartmentById(des.getDepartment_id());
		}
		else {
			this.department = des;
		}
		
//		this.binder.setBean(this.department);
		this.department_name.focus();
		super.departmentDialog.add(super.department_name, super.addBtn);
		return this.department;
	}
	
	void listDepartments(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			this.departmentGrid.setItems(this.departmentService.listDepartment());			
			
	    } else {
	    	this.departmentGrid.setItems(this.departmentService.getDepartmentByName(filterText));
	    }
    }	
}
