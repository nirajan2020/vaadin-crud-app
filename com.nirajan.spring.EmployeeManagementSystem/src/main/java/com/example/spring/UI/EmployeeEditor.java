package com.example.spring.UI;

import com.example.spring.entities.Department;
import com.example.spring.entities.Designation;
import com.example.spring.entities.Employee;
//import com.example.spring.entities.Employee.comboBoxOptions;
import com.example.spring.entities.Employee.options;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@SpringComponent
@UIScope
public class EmployeeEditor{	
	
	/** The name of the components should match with the field names of an entity class   **/
	RadioButtonGroup<Employee.options> salutation;
	TextField name;
	NumberField salary;
//	ComboBox<Employee.comboBoxOptions> designation = new ComboBox<>("Designation");
	ComboBox<Designation> designation_name;
	ComboBox<Department> department_name;
	TextArea description;	
	
	Button save;
	Button cancel;
	Button update;
	Button delete;
	VerticalLayout vLayout;
	HorizontalLayout actions;
	Dialog dialog;
	
	public EmployeeEditor() {
		salutation = new RadioButtonGroup<>();
		name = new TextField("Full Name");
		salary = new NumberField("Salary");
		designation_name = new ComboBox<Designation>("Designation");
		department_name = new ComboBox<>("Department");
		description = new TextArea("Job Description");	
		save = new Button("Save", VaadinIcon.CHECK.create());
		cancel = new Button("Cancel");
		update = new Button("Update");
		delete = new Button("Delete", VaadinIcon.TRASH.create());
		vLayout = new VerticalLayout();
		actions = new HorizontalLayout(save, update, delete, cancel);
		dialog = new Dialog();
		salutation.setLabel("Salutation");
		salutation.setItems(options.values());
		salutation.addThemeName("horizon"); // by default horizontal
		description.setPlaceholder("Write here ...");
		description.setHeight("100px");
		description.setMaxLength(10000);

		vLayout.add(salutation, name, salary, designation_name, department_name, description, actions);	
			
		vLayout.setSpacing(true);
		save.getElement().getThemeList().add("primary");
		delete.getElement().getThemeList().add("error");
	}
}
