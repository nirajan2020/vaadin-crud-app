package com.example.spring.UI;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class MainView extends VerticalLayout{
    TextField filter;
    Button addNewEmployeeBtn, addNewDesignationBtn, addNewDepartmentBtn;   
    
	public MainView() {
		this.filter = new TextField();
		this.addNewEmployeeBtn = new Button("New employee", VaadinIcon.PLUS.create());
		this.addNewDesignationBtn = new Button("Add Designation", VaadinIcon.PLUS.create());
		this.addNewDepartmentBtn = new Button("Add Department", VaadinIcon.PLUS.create());	 
		this.filter.setPlaceholder("Filter by name");		

	}
}
