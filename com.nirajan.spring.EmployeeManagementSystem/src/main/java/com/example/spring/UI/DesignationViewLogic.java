package com.example.spring.UI;

import org.springframework.util.StringUtils;

import com.example.spring.entities.Designation;
import com.example.spring.entities.Employee;
import com.example.spring.service.DesignationService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;

public class DesignationViewLogic extends DesignationView{
	private DesignationService designationService;
	private Designation designation;
	private Grid<Designation> designationGrid;
	private Div content;
	private Binder<Designation> binder;
	private HorizontalLayout actions, operations;
	private VerticalLayout verticalLayout;
	
	public DesignationViewLogic(DesignationService designationService) {
		this.designationService = designationService;
		this.designation = new Designation(null);
		this.designationGrid = new Grid<>(Designation.class);
		this.binder = new Binder<>(Designation.class);
		this.content = new Div(this.designationGrid); 
		this.actions = new HorizontalLayout(super.designationFilter, super.addNewDesignationBtn);
		this.operations = new HorizontalLayout(super.addBtn, super.updateBtn, super.deleteBtn, super.cancelBtn);
		this.verticalLayout = new VerticalLayout();
		
		this.binder.bindInstanceFields(this);
		this.binder.setBean(this.designation);
		
        this.content.setSizeFull();
        this.designationGrid.setHeight("200px");
        this.designationGrid.setColumns("designation_id", "designation_name");            
        
        this.verticalLayout.setSpacing(true);
        this.verticalLayout.add(super.designation_name, this.operations);
        super.designationDialog.add(this.verticalLayout);
        
        add(this.actions, this.content);
		
		listDesignations(null);
		
		super.designationFilter.setValueChangeMode(ValueChangeMode.EAGER);// read the api
		super.designationFilter.addValueChangeListener(e -> listDesignations(e.getValue())); 
		
		super.addNewDesignationBtn.addClickListener(e -> {	
			editDesignation(new Designation(null));
			super.designationDialog.open();
		});
		
		super.addBtn.addClickListener(e -> {				
			this.designationService.addDesignation(this.designation);
			listDesignations(null);
			super.designationDialog.close();
		});
		
		super.updateBtn.addClickListener(e -> {	
			this.designationService.updateDesignation(this.designation);
			listDesignations(null);
			super.designationDialog.close();
		});
		
		super.cancelBtn.addClickListener(e -> {				
			super.designationDialog.close();
		});
		
		super.deleteBtn.addClickListener(e -> {	
			if(this.designation.getDesignation_id() != null) {
				this.designationService.removeDesignation(this.designation.getDesignation_id());
				listDesignations(null);
				super.designationDialog.close();
			}
		});
		
		this.designationGrid.addColumn(new ComponentRenderer<>(Designation -> {
		    // button for updating an employee details to backend
			editDesignation(Designation); 
		    Button update = new Button("Edit", event -> {    			    	
		    	super.designationDialog.open();
		    	super.addBtn.setEnabled(false);
		    });

		    // button that removes an employee 
		    Button remove = new Button("Remove", event -> {
				this.designationService.removeDesignation(this.designation.getDesignation_id());
				listDesignations(null);
		    });

		    // layouts for placing the text field on top of the buttons
		    HorizontalLayout buttons = new HorizontalLayout(update, remove);
		    return new VerticalLayout( buttons);
		})).setHeader("Actions");
		
		this.designationGrid.addItemDoubleClickListener(listener-> 
        { 
        	editDesignation(listener.getItem());         
        	super.designationDialog.open();
         });
	}
	
	public Designation editDesignation (Designation des) {	
		if (des == null) {
			super.designationDialog.open();
			return null;
		}
		final boolean persisted = des.getDesignation_id() != null;
		if (persisted) {
			this.designation = this.designationService.getDesignationById(des.getDesignation_id());
		}
		else {
			this.designation = des;
		}
		
		this.binder.setBean(this.designation);
		this.designation_name.focus();
		super.designationDialog.add(super.designation_name, super.addBtn);
		return this.designation;
	}
	
	void listDesignations(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			this.designationGrid.setItems(this.designationService.listDesignation());			
			
	    } else {
	    	this.designationGrid.setItems(this.designationService.getDesignationByName(filterText));
	    }
    }	
}
