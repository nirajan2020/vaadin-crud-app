package com.example.spring.UI;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@Route("designationView")
@SpringComponent
@UIScope
public class DesignationView extends VerticalLayout {
    TextField designationFilter, designation_name;
    Button addNewDesignationBtn, addBtn, updateBtn, cancelBtn, deleteBtn;
    Dialog designationDialog;
    
	public DesignationView() {
		this.designationFilter = new TextField();
		this.designation_name = new TextField("Designation");
		this.addBtn = new Button("Save");
		this.updateBtn = new Button("Update");
		this.deleteBtn = new Button("Delete");
		this.cancelBtn = new Button("Cancel");
		this.addNewDesignationBtn = new Button("New Designation", VaadinIcon.PLUS.create());
		this.designationDialog = new Dialog();		
		 
        this.designationFilter.setPlaceholder("Search by designation name");		
	}
}
