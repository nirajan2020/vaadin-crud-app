package com.example.spring.UI;

import com.example.spring.entities.Employee;
import com.example.spring.service.DepartmentService;
import com.example.spring.service.DesignationService;
import com.example.spring.service.NewEmployeeService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

@Route(value="myhome")
public class MainViewLogic extends MainView{
	private EmployeeEditorLogic employeeEditorLogic;	
	private NewEmployeeService newEmployeeService;
	private DesignationService designationService;
	private DepartmentService departmentService;
	Grid<Employee> grid;
    private HorizontalLayout actions;
    private Div content;

	public MainViewLogic(NewEmployeeService newEmployeeService, DesignationService designationService, DepartmentService departmentService) {
		this.newEmployeeService = newEmployeeService;
		this.designationService = designationService;
		this.departmentService = departmentService;
		this.employeeEditorLogic = new EmployeeEditorLogic(this.newEmployeeService, this.designationService, this.departmentService, this);
		this.grid = new Grid<>(Employee.class);
		
		this.content = new Div(grid); 
        this.content.setSizeFull();
		
		this.grid.setHeight("200px");
		// grid column name should match with the field names of an entity class
		this.grid.setColumns("id", "salutation", "name", "salary","description");
		this.grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);	

		actions = new HorizontalLayout(filter, addNewEmployeeBtn, addNewDesignationBtn, addNewDepartmentBtn);
		add(actions, content);
		
		this.employeeEditorLogic.listEmployees(null);
		
		filter.setValueChangeMode(ValueChangeMode.EAGER);// read the api		
		filter.addValueChangeListener(e -> this.employeeEditorLogic.listEmployees(e.getValue())); 
		
	    addNewEmployeeBtn.addClickListener(e -> {
	    	this.employeeEditorLogic.populateComboBox();
		});
        
        grid.addItemDoubleClickListener(listener-> 
        { 
        	this.employeeEditorLogic.editEmployee(listener.getItem());
        	this.employeeEditorLogic.dialog.open();
        	this.employeeEditorLogic.save.setEnabled(false);
         });

        addNewDesignationBtn.addClickListener(e -> {	
        	UI.getCurrent().navigate("designationView"); // navigate to a new view
		});
        
        addNewDepartmentBtn.addClickListener(e -> {	
        	UI.getCurrent().navigate("departmentView"); // navigate to a new view
		});
        		
        grid.addColumn(new ComponentRenderer<>(Employee -> {
        	this.employeeEditorLogic.editEmployee(Employee);        	
		    Button update = new Button("Edit", event -> {	
		    	this.employeeEditorLogic.editEmployee(Employee);
		    	this.employeeEditorLogic.dialog.open();
		    });

		    Button remove = new Button("Remove", event -> {
				this.newEmployeeService.removeEmployee(Employee.getId());
				this.employeeEditorLogic.listEmployees(null);
		    });

		    HorizontalLayout buttons = new HorizontalLayout(update, remove);
		    return new VerticalLayout( buttons);
		})).setHeader("Actions");
	}	

}
