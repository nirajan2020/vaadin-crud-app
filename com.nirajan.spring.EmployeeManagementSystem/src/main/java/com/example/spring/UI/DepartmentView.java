package com.example.spring.UI;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@Route("departmentView")
@SpringComponent
@UIScope
public class DepartmentView extends VerticalLayout{

	TextField departmentFilter, department_name;
    Button addNewDepartmentBtn, addBtn, updateBtn, cancelBtn, deleteBtn;
    Dialog departmentDialog;
    
	public DepartmentView() {
		this.departmentFilter = new TextField();
		this.department_name = new TextField("Department");
		this.addBtn = new Button("Save");
		this.updateBtn = new Button("Update");
		this.deleteBtn = new Button("Delete");
		this.cancelBtn = new Button("Cancel");
		this.addNewDepartmentBtn = new Button("New Department", VaadinIcon.PLUS.create());
		this.departmentDialog = new Dialog();		
		 
        this.departmentFilter.setPlaceholder("Search by department name");		
	}
}
