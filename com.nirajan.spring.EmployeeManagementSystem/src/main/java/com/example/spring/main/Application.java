package com.example.spring.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.flow.spring.annotation.EnableVaadin;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.spring.*") // important
@ComponentScan(basePackages = "com.example.spring.*") // important
@EntityScan("com.example.spring.*") //important  
@EnableVaadin({"com.example.spring.UI"}) // important
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
