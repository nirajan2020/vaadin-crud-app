package com.example.spring.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_department")
public class Department {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long department_id;  
	
	private String department_name;
	
//	@OneToMany(mappedBy="department", cascade = CascadeType.ALL)
//	private List<Employee>employees;	

	public Department() {
    }
    
	public Department(String department_name) {
		this.department_name = department_name;
//		employees = new ArrayList<Employee>();
	}

	public void setDepartment_id(Long department_id) {
		this.department_id = department_id;
	}
	
	public Long getDepartment_id() {
		return department_id;
	}	
	
	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

}
