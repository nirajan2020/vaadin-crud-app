package com.example.spring.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_designation")
public class Designation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long designation_id;  
	private String designation_name;
	
//	@OneToMany(mappedBy="designation", cascade = CascadeType.ALL)
//	private List<Employee>employees;

	public Designation() {
    }
    
	public Designation( String designation_name) {
		this.designation_name = designation_name;
//		employees = new ArrayList<Employee>();
	}
	
	public Long getDesignation_id() {
		return designation_id;
	}
	public void setDesignation_id(Long designation_id) {
		this.designation_id = designation_id;
	}
	public String getDesignation_name() {
		return designation_name;
	}
	public void setDesignation_name(String designation_name) {
		this.designation_name = designation_name;
	}
	
//    public List<Employee> getEmployees() {
//		return this.employees;
//	}
//
//	public void setEmployees(List<Employee> employees) {
//		this.employees = employees;
//	}
	
}
