package com.example.spring.entities;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
	List<Employee> findByNameStartsWithIgnoreCase(String name);
	
	@Query("select emp from Employee emp " +
		      "where lower(emp.name) like lower(concat('%', :searchTerm, '%')) ") 
		    List<Employee> search(@Param("searchTerm") String searchTerm);
}
