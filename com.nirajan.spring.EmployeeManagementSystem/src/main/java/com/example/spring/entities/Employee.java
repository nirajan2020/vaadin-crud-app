package com.example.spring.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Employee {
	public enum options {
	    Mr, Ms, Mrs
	}	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;  
	private String name;
	private double salary;    
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private Employee.options salutation;
	
	@ManyToOne
	@JoinColumn(name ="designation_id")
	private Designation designation;
	
	@ManyToOne
	@JoinColumn(name ="department_id")
	private Department department;

	private String description;	
	
	public Employee(String name, double salary, @NotNull Employee.options salutation,
			@NotNull Designation designation, String description, @NotNull Department department) {
		this.name = name;
		this.salary = salary;
		this.salutation = salutation;
		this.designation = designation;
		this.description = description;
		this.department = department;
	}
	
    public Employee() {
    }
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public options getSalutation() {
		return salutation;
	}

	public void setSalutation(options salutation) {
		this.salutation = salutation;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public Designation getDesignation() {
		return this.designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}
	
	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
}
